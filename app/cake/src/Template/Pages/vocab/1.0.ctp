<div class="text-left">
    <section class="border-bottom mt-3">
        <h2 id="identifier">Identifiers</h2>
        <p>Various identifiers used throughout the CDLI database.</p>
        <dl>
            <dt id="identifier_cdli">cdli</dt>
            <dl>
                CDLI numbers, formatted as <code>P######</code> (where <code>#</code>
                are numbers 0–9). Issued for artifacts by the CDLI.
            </dl>

            <dt id="identifier_designation">designation</dt>
            <dl>Designations. Issued for artifacts.</dl>

            <dt id="identifier_accession">accession</dt>
            <dl>Accession numbers. Issued for artifacts.</dl>

            <dt id="identifier_ark">ark</dt>
            <dl>
                <a href="https://www.arks.org/e/ark_ids.html">Archive Resource Keys</a>,
                formatted like <code>[http://NMA/]ark:/NAAN/Name[Qualifier]</code>
                though in the database, only the NAAN (21198) and the Name are used.
            </dl>

            <dt id="identifier_museum">museum</dt>
            <dl>Museum numbers.</dl>

            <dt id="identifier_findspot">findspot</dt>
            <dl>Findspot square.</dl>

            <dt id="identifier_excavation">excavation</dt>
            <dl>Excavation numbers.</dl>

            <dt id="identifier_composite">composite</dt>
            <dl>
                Composite numbers, formatted as <code>Q######</code> (where
                <code>#</code> are numbers 0–9). Actual formatting might vary.
                Issued for composites by [QCat](http://oracc.museum.upenn.edu/qcat/).
            </dl>

            <dt id="identifier_seal">seal</dt>
            <dl>
                Seal numbers, formatted as <code>S######</code> (where <code>#</code>
                are numbers 0–9). Actual formatting might vary. Issued for seals.
            </dl>

            <dt id="identifier_iso-639">iso-639</dt>
            <dl>ISO 639-1:2002 language codes.</dl>
        </dl>
    </section>

    <section class="border-bottom mt-3">
        <h2 id="type">Types</h2>
        <p>Various classifications used throughout the CLDI database.</p>
        <dl>
            <dt id="type_artifact_type">artifact_type</dt>
            <dl>Artifact Type.</dl>

            <dt id="type_genre">genre</dt>
            <dl>Genre.</dl>

            <dt id="type_aspect">aspect</dt>
            <dl>Material aspect.</dl>

            <dt id="type_color">color</dt>
            <dl>Material color.</dl>
        </dl>
    </section>

    <section class="border-bottom mt-3">
        <h2 id="dimension">Dimensions</h2>
        <p>Dimensions used to describe physical artifacts.</p>
        <dl>
            <dt id="dimension_height">height</dt>
            <dl>Artifact height. Usually the secondary reading direction.</dl>

            <dt id="dimension_width">width</dt>
            <dl>Artifact width. Usually the primary reading direction.</dl>

            <dt id="dimension_thickness">thickness</dt>
            <dl>Artifact thickness. The remaining spatial dimension.</dl>

            <dt id="dimension_weight">weight</dt>
            <dl>Artifact weight.</dl>
        </dl>
    </section>

    <section class="border-bottom mt-3">
        <h2 id="unit">Units</h2>
        <dl>
            <dt id="mm">mm</dt>
            <dl>Millimeter.</dl>

            <dt id="g">g</dt>
            <dl>Gram.</dl>
        </dl>
    </section>
</div>
