<?xml version="1.0"?>
<rdf:RDF xmlns="http://www.w3.org/2002/07/owl#"
     xml:base="http://www.w3.org/2002/07/owl"
     xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
     xmlns:owl="http://www.w3.org/2002/07/owl#"
     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     xmlns:xml="http://www.w3.org/XML/1998/namespace"
     xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
     xmlns:cdli="https://cdli.ucla.edu/docs/vocab/0.1#"
     xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <Ontology/>
    


    <!-- 
    ///////////////////////////////////////////////////////////////////////////////////////
    //
    // Classes
    //
    ///////////////////////////////////////////////////////////////////////////////////////
     -->

    


    <!-- http://www.cidoc-crm.org/cidoc-crm/E42_Identifier -->

    <Class rdf:about="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
    


    <!-- http://www.cidoc-crm.org/cidoc-crm/E54_Dimension -->

    <Class rdf:about="http://www.cidoc-crm.org/cidoc-crm/E54_Dimension"/>
    


    <!-- http://www.cidoc-crm.org/cidoc-crm/E55_Type -->

    <Class rdf:about="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
    


    <!-- http://www.cidoc-crm.org/cidoc-crm/E58_Measurement_Unit -->

    <Class rdf:about="http://www.cidoc-crm.org/cidoc-crm/E58_Measurement_Unit"/>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#dimension_height -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#dimension_height">
        <rdfs:subClassOf rdf:resource="https://cdli.ucla.edu/docs/vocab/0.1#dimension_length"/>
        <rdfs:comment xml:lang="en">Artifact height. Usually the secondary reading direction.</rdfs:comment>
        <rdfs:label xml:lang="en">Height</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#dimension_length -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#dimension_length">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E54_Dimension"/>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#dimension_thickness -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#dimension_thickness">
        <rdfs:subClassOf rdf:resource="https://cdli.ucla.edu/docs/vocab/0.1#dimension_length"/>
        <rdfs:comment xml:lang="en">Artifact thickness. The remaining spatial dimension.</rdfs:comment>
        <rdfs:label xml:lang="en">Thickness</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#dimension_weight -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#dimension_weight">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E54_Dimension"/>
        <rdfs:comment xml:lang="en">Artifact weight.</rdfs:comment>
        <rdfs:label xml:lang="en">Weight</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#dimension_width -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#dimension_width">
        <rdfs:subClassOf rdf:resource="https://cdli.ucla.edu/docs/vocab/0.1#dimension_length"/>
        <rdfs:comment xml:lang="en">Artifact width. Usually the primary reading direction.</rdfs:comment>
        <rdfs:label xml:lang="en">Width</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#g -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#g">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E58_Measurement_Unit"/>
        <rdfs:label rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Gram</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_accession -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_accession">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Accession number</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_ark -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_ark">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">Formatted like [http://NMA/]ark:/NAAN/Name[Qualifier] though in the database, only the NAAN (21198) and the Name are used.</rdfs:comment>
        <rdfs:label xml:lang="en">Archive Resource Key</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_cdli -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_cdli">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">CDLI numbers, formatted as P###### (where # are numbers 0–9). Issued for artifacts by the CDLI.</rdfs:comment>
        <rdfs:label xml:lang="en">CDLI number</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_composite -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_composite">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">Composite numbers, formatted as Q###### (where # are numbers 0–9). Actual formatting might vary. Issued for composites by ORACC&apos;s QCat.</rdfs:comment>
        <rdfs:label xml:lang="en">Composite number</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_designation -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_designation">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Designation</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_excavation -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_excavation">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Excavation number</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_findspot -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_findspot">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Findspot square</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_iso-639 -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_iso-639">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment rdf:datatype="http://www.w3.org/2001/XMLSchema#string">ISO 639-1:2002 language codes.</rdfs:comment>
        <rdfs:label xml:lang="en">Inline language code</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_museum -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_museum">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:label xml:lang="en">Museum number</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#identifier_seal -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#identifier_seal">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E42_Identifier"/>
        <rdfs:comment xml:lang="en">Seal numbers, formatted as S###### (where # are numbers 0–9). Actual formatting might vary. Issued for seals.</rdfs:comment>
        <rdfs:label xml:lang="en">Seal number</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#mm -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#mm">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E58_Measurement_Unit"/>
        <rdfs:label rdf:datatype="http://www.w3.org/2001/XMLSchema#string">Millimeter</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#type_artifact_type -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#type_artifact_type">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
        <rdfs:label xml:lang="en">Artifact type</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#type_aspect -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#type_aspect">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
        <rdfs:label xml:lang="en">Material aspect</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#type_color -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#type_color">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
        <rdfs:label xml:lang="en">Material color</rdfs:label>
    </Class>
    


    <!-- https://cdli.ucla.edu/docs/vocab/0.1#type_genre -->

    <Class rdf:about="https://cdli.ucla.edu/docs/vocab/0.1#type_genre">
        <rdfs:subClassOf rdf:resource="http://www.cidoc-crm.org/cidoc-crm/E55_Type"/>
        <rdfs:label xml:lang="en">Genre</rdfs:label>
    </Class>
</rdf:RDF>



<!-- Generated by the OWL API (version 4.5.13) https://github.com/owlcs/owlapi -->

