<div class="boxed mx-0">
    <div class="capital-heading">
        <?= __('Artifact Information:') ?>
        <?= $this->Html->link('View Artifact', [
            'prefix' => false,
            'controller' => 'Artifacts',
            'action' => 'view',
            $artifact->id
        ], [
            'class' => 'btn btn-action',
            'style' => "float: right;"
        ]) ?>
    </div>
    <table class="table-bootstrap mx-0">
        <tr>
            <th><?= __('Artifact Identifier') ?></th>
            <td>
                <?= $this->Html->link(h('P'.substr("00000{$artifact->id}", -6)), ['controller' => 'Artifacts', 'action' => 'view', $artifact->id]) ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Designation') ?></th>
            <td>
                <?= h($artifact->designation) ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Museum Collections') ?></th>
            <td>
            <?php foreach ($artifact->collections as $i => $collection): ?>
                <?php if ($i != 0): ?>
                    <?= '; ' ?>
                <?php endif ?>
                <?= $this->Html->link($collection->collection, ['controller' => 'Collections', 'action' => 'view', $collection->id]) ?>
            <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Period') ?></th>
            <td>
                <?= $this->Html->link($artifact->period->period, ['controller' => 'Periods', 'action' => 'view', $artifact->period->id]) ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Provenience') ?></th>
            <td>
                <?= $this->Html->link($artifact->provenience->provenience, ['controller' => 'Proveniences', 'action' => 'view', $artifact->provenience->id]) ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Artifact Type') ?></th>
            <td>
                <?= $this->Html->link($artifact->artifact_type->artifact_type, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
            </td>
        </tr>
        <tr>
            <th><?= __('Accession') ?></th>
            <td>
                <?= $this->Html->link($artifact->accession, ['controller' => 'ArtifactTypes', 'action' => 'view', $artifact->artifact_type->id]) ?>
            </td>
        </tr>
    </table>
</div>


