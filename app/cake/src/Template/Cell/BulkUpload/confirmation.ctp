<?php if (isset($entities) and isset($error_list)): ?>
    <?= $this->Form->create(null, [
    'url' => [
        'action' => 'add/bulk'
    ]
]) ?>
        <?= $this->Form->input('entities_serialize', ['type' => 'hidden', 'value' => base64_encode(serialize($entities))]) ?>
        <?= $this->Form->input('error_list_serialize', ['type' => 'hidden', 'value' => base64_encode(serialize($error_list))]) ?>
        <?= $this->Form->input('header_serialize', ['type' => 'hidden', 'value' => base64_encode(serialize($header))]) ?>
    <div class="justify-content-md-center row">
        <table>
            <tr>
                <td><?= $this->Form->submit('Proceed', ['class' => 'btn btn-primary']); ?></td>
                <td><?= $this->Html->link('Cancel', [
                    'action' => 'add', 'bulk'],
                    ['class' => 'btn btn-primary']) ?></td>
            </tr>
        </table>
    </div>
        <?= $this->Form->end() ?>
    
<?php endif; ?>
