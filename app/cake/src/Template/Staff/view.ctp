<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Staff $staff
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Staff'), ['action' => 'edit', $staff->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Staff'), ['action' => 'delete', $staff->id], ['confirm' => __('Are you sure you want to delete # {0}?', $staff->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Staff'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Staff'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Staff Types'), ['controller' => 'StaffTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Staff Type'), ['controller' => 'StaffTypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="staff view large-9 medium-8 columns content">
    <h3><?= h($staff->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Author') ?></th>
            <td><?= $staff->has('author') ? $this->Html->link($staff->author->id, ['controller' => 'Authors', 'action' => 'view', $staff->author->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Staff Type') ?></th>
            <td><?= $staff->has('staff_type') ? $this->Html->link($staff->staff_type->id, ['controller' => 'StaffTypes', 'action' => 'view', $staff->staff_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sequence') ?></th>
            <td><?= h($staff->sequence) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($staff->id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Cdli Title') ?></h4>
        <?= $this->Text->autoParagraph(h($staff->cdli_title)); ?>
    </div>
    <div class="row">
        <h4><?= __('Contribution') ?></h4>
        <?= $this->Text->autoParagraph(h($staff->contribution)); ?>
    </div>
</div>
