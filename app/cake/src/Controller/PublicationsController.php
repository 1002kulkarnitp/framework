<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Publications Controller
 *
 * @property \App\Model\Table\PublicationsTable $Publications
 *
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PublicationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Bibliography');
        $this->loadComponent('TableExport');
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('LinkedData');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['EntryTypes', 'Journals', 'Abbreviations', 'Authors', 'Editors']
        ];
        $publications = $this->paginate($this->Publications);

        $this->set(compact('publications'));
        $this->set('_serialize', 'publications');
    }

    /**
     * View method
     *
     * @param string|null $id Publication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Allow only users with proper permissions to view private artifacts
        $condition = (!$this->GeneralFunctions->checkIfRolesExists([1,4])) ?  ['Artifacts.is_public' => true] : [];
        $publication = $this->Publications->get($id, [
            'contain' => [
                'Artifacts' => [
                    'Proveniences',
                    'Periods',
                    'ArtifactTypes',
                    'Collections',
                    'sort' => 'Artifacts.id',
                    'conditions' => $condition
                ],
                'EntryTypes',
                'Journals',
                'Editors' => [
                    'sort' => ['EditorsPublications.sequence' => 'ASC']
                    ],
                'Authors' => [
                    'sort' => ['AuthorsPublications.sequence' => 'ASC']
                    ]
                ]
        ]);

        $is_admin = $this->GeneralFunctions->checkIfRolesExists([1]);
        $this->set(compact('publication', 'is_admin'));
        $this->set('_serialize', 'publication');
    }
}
