<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ArtifactsShadow Controller
 *
 * @property \App\Model\Table\ArtifactsShadowTable $ArtifactsShadow
 *
 * @method \App\Model\Entity\ArtifactsShadow[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsShadowController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts']
        ];
        $artifactsShadow = $this->paginate($this->ArtifactsShadow);

        $this->set(compact('artifactsShadow'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifacts Shadow id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $artifactsShadow = $this->ArtifactsShadow->get($id, [
            'contain' => ['Artifacts']
        ]);

        $this->set('artifactsShadow', $artifactsShadow);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $artifactsShadow = $this->ArtifactsShadow->newEntity();
        if ($this->request->is('post')) {
            $artifactsShadow = $this->ArtifactsShadow->patchEntity($artifactsShadow, $this->request->getData());
            if ($this->ArtifactsShadow->save($artifactsShadow)) {
                $this->Flash->success(__('The artifacts shadow has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts shadow could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsShadow->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('artifactsShadow', 'artifacts'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifacts Shadow id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $artifactsShadow = $this->ArtifactsShadow->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifactsShadow = $this->ArtifactsShadow->patchEntity($artifactsShadow, $this->request->getData());
            if ($this->ArtifactsShadow->save($artifactsShadow)) {
                $this->Flash->success(__('The artifacts shadow has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifacts shadow could not be saved. Please, try again.'));
        }
        $artifacts = $this->ArtifactsShadow->Artifacts->find('list', ['limit' => 200]);
        $this->set(compact('artifactsShadow', 'artifacts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifacts Shadow id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $artifactsShadow = $this->ArtifactsShadow->get($id);
        if ($this->ArtifactsShadow->delete($artifactsShadow)) {
            $this->Flash->success(__('The artifacts shadow has been deleted.'));
        } else {
            $this->Flash->error(__('The artifacts shadow could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
