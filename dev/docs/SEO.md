# SEO development guide

Guide on how to work with SEO tags in the framework. Currently, three view block
variables are used:

  - `title`,
  - `image` (URL), and
  - `description`

For pages fetched with the view action, the title is taken from the display field
of the entity marked for serialization (with `_serialize`). To change the title,
or set the image or description use [`View::assign()`](https://api.cakephp.org/3.9/class-Cake.View.View.html#assign)
anywhere in the template. In the layout template, i.e. `app/cake/src/Template/Layout/default.ctp`,
these variables are taken and transformed into metadata tags. The following tags are
generated:

  - `twitter:card`: "summary" or "summary_large_image" if an image URL is set
  - `og:image`: image URL
  - `og:title`: title
  - `description`, `og:description`: description
  - `og:url`: current page URL (currently hard-coded to use the `cdli.ucla.edu`
    domain)
  - `og:site_name`: "CDLI"
  - `twitter:site`: "[@cdli_news](https://twitter.com/cdli_news)"

All OpenGraph tags (https://ogp.me/) use the attributes `property` and `content`;
other tags that are used use `name` and `content`.
