#!/bin/bash

# Install script for CDLI Framework
#
# Author : Vedant Wakalkar (@karna98) 
# version 1.1

# Virtual Environment Name
CDLI_ENV_NAME="cdli_env"

# Full path to parent directory of setup.sh (executed)
SETUP_SH_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Virtual Environment Path
CDLI_ENV_PATH=$SETUP_SH_DIR/$CDLI_ENV_NAME

# Check if setup.sh is executed outside 'framework' folder 
if [ "${PWD##*/}" = "framework" ];
then
    printf -- "\nERROR : Working in wrong directory ($SETUP_SH_DIR).\n"
    printf -- "\nWait I will make it right for you.\nAnyway what am I for :-)\n"
    newPath=$(dirname "$SETUP_SH_DIR")/setup.sh
    if [ -f "$newPath" ]
    then
        cd ..
        ./setup.sh
    else
        cp $SETUP_SH_DIR/setup.sh $newPath
        chmod +x $newPath
        if [ -f "$newPath" ]
        then
            cd ..
            ./setup.sh
        else
            printf -- "\n Error in copying file!!\n Manually copy setup.sh from '~/CDLI/framework' folder to '~/CDLI' "
        fi
    fi
    exit 0
fi 

# Display Message 
stage_start() {
    printf -- "\n*------------------------------*"
    printf -- "\n*------  Stage-$1 Setup  -------*"
    printf -- "\n*------------------------------*\n"
}

stage_finished() {
    printf -- "\n*------------------------------*"
    printf -- "\n*--- Stage-$1 Setup Finished ---*"
    printf -- "\n*------------------------------*\n"
}

stage_aborted() {
    printf -- "\n*------------------------------*"
    printf -- "\n*------ Stage-$1 Aborted -------*"
    printf -- "\n*------------------------------*\n"
    exit 1
}

# Display activate virtual environment message 
activate_venv_msg() {
    if [ $1 != 0 ]
    then 
        printf -- "\nERROR: Activate virtual environment ("$CDLI_ENV_NAME") and run setup.sh for 'Stage-$1' again."
    fi
    printf -- "\n-- Activate virtual environment by running following command :"
    printf -- "\n\n   > source $CDLI_ENV_PATH/bin/activate"
    printf -- "\n\n                OR                     "
    printf -- "\n\n   > source $CDLI_ENV_NAME/bin/activate\n"
}

# Generates dbSetup.sh to be executed inside 'mariadb' docker to upload sql
# Note: Keep the indentation of this fucntion as it is.
docker_dbscript() {
read -r -d '' DB_SETUP_SCRIPT <<- EOM
#!/bin/bash
while true;
do
    printf "\n-- Checking if Database 'cdli_db' exists"
    if  [ ! -d /var/lib/mysql/cdli_db ] 
    then
        printf "\n-- Database 'cdli_db' do not exists !!"
        printf "\n-- Creating Empty Database 'cdli_db'\n"
        mysql -u root --execute "CREATE database cdli_db" 
        printf "\n-- Successfully created database 'cdli_db'"
        printf "\n-- Uploading "cdli_db.sql" to database 'cdli_db'      ETA : 10 to 30 min"
        printf "\nStart Time : %(%H:%M:%S)T \nUploading ..." \$(date +"%t") 
        mysql cdli_db < /tmp/cdli_db.sql; 
        printf "\nEnd Time : %(%H:%M:%S)T" \$(date +"%t")    
        printf "\n-- Data uploaded sucessfully to database 'cdli_db'"
        break
    else
        printf "\n-- Database 'cdli_db' already exists !!\n"
        read -p "Do you want to upload new sql ? [y|N] " yesNo

        if [[ \$yesNo =~ ^(y|Y)\$ ]]
        then
            printf "\n-- Dropping "cdli_db" database ..\n"
            mysql -u root --execute "DROP DATABASE cdli_db"
            printf "\n-- Sucessfully dropped 'cdli_db' database !!"
            ./dbSetup.sh
            exit 0
        else
            break
        fi
    fi
done
rm tmp/cdli_db.sql
printf "\n-- Exiting 'cdlidev_mariadb_1' shell"
EOM

echo "$DB_SETUP_SCRIPT" > $SETUP_SH_DIR/.tmp/dbSetup.sh
}

# Stage-1 Setup
stage_1(){

    # 1.1 Detect Virtual Environment
    printf -- "\n1. Detecting 'virtualenv'\n"
    if ! [ -x "$(command -v virtualenv)" ]; 
    then
        printf -- "\n-- No 'virtualenv' command found."
        printf -- "\n-- Installing 'virtualenv' ...\n" 
        sudo apt-get install python3-pip
        pip3 install virtualenv 
        printf -- "\n-- Successfully installed 'virtualenv'."  
    fi
    printf -- "\n-- 'virtualenv' detected!!" 
    printf -- "\n-- 'virtualenv' version %s" $(virtualenv --version)

    # 1.2 Detect git 
    printf -- "\n\n2. Detecting 'git'\n"
    if ! [ -x "$(command -v git)" ]; 
    then
        printf -- "\n-- No 'git' command found."
        printf -- "\n-- Installing 'git' ...\n" 
        sudo apt install git 
        printf -- "\n-- Successfully installed 'git'.\n"
    fi
    printf -- "\n-- 'git' detected!!" 
    printf -- "\n-- %s %s %s" $(git --version)
    
    # 1.3 Detect docker
    printf -- "\n\n3. Detecting 'docker'\n"
    if ! [ -x "$(command -v docker)" ]; 
    then
        printf -- "\nERROR : No 'docker' command found."
        printf -- "\nSolution : 'docker' by following the instructions on https://docs.docker.com/v17.12/install/."
        printf -- "\n-- Restart the setup (Stage 1) after installing 'docker'.\nExiting setup.\n"
        stage_aborted 1
    fi
    printf -- "\n-- 'docker' detected!!\n"
    LoggedUser=$USER
    if [ "$USER" = "root" ];
    then
        LoggedUser=$(users)
    fi 
    sudo chmod g+rwx "/home/$LoggedUser/.docker" -R
    printf -- "-- %s %s %s %s %s" $(docker --version)
    sudo usermod -aG docker $LoggedUser
    printf -- "\n-- Added "$LoggedUser" to docker group!!"

    # 1.4 Create Virtual Environment 
    printf -- "\n\n4. Creating Virtual Environment"
    printf -- "\n\n-- virtualenv name : "$CDLI_ENV_NAME
    printf -- "\n-- virtualenv path : "$CDLI_ENV_PATH"\n"

    if [ -d "$CDLI_ENV_PATH" ];
    then
        printf -- "\n-- Environment ("$CDLI_ENV_NAME") exists !!\n"
        read -p "   Do you want to setup virtual environment by removing existing one ? [y|N] " yesNo
        if [[  $yesNo =~ ^(y|Y)$ ]]
        then
            printf --  "\n"
            sudo rm -r $CDLI_ENV_PATH
        fi 
    fi

    if [ ! -d "$CDLI_ENV_PATH" ];
    then
        virtualenv $CDLI_ENV_PATH --python=python3 
        printf -- "\n-- Virtual Environment ("$CDLI_ENV_NAME") created successfully !!"
    fi

    activate_venv_msg 0

    printf -- "\nAfter activating Virtual Environment, run setup.sh for Stage-2 Setup.\n"
}

# Stage-2 Setup
stage_2() {

    # 2.1 Detect Virtual Environment
    printf -- "\n1. Detecting (cdli_env) virtual environment\n"
    if [ "$VIRTUAL_ENV" == $CDLI_ENV_PATH ];
    then
        printf -- "\n-- Detected Virtual Environment : "$CDLI_ENV_NAME"\n"
    else
        activate_venv_msg 2
        stage_aborted 2
    fi
        
    # 2.2 Clone Repository
    printf -- "\n2. Clone repository\n"
    if [ -d "framework" ]
    then 
        printf -- "\n-- 'framework' already exists !!\n"
        read -p "Do you want to remove current 'framework' and clone again ? [y|N] " yesNo

        if [[ $yesNo =~ ^(y|Y)$ ]]
        then 
            sudo rm -r framework            
        fi
    fi

    if [ ! -d "framework" ]
    then
        printf -- "\n-- Initiation Cloning ..\n"
        git clone https://gitlab.com/cdli/framework --depth 1
        printf -- "\n-- Repository Cloned \n"
    else 
        printf -- "\n-- Skipping Clonning Step !!\n"
    fi

    # 2.3 Set up permissions for cloned repository (locally)
    printf -- "\n3. Sets up extended ACL permissions on the 'framework'\n"
    printf -- "\n-- Setting up permissions ..\n"
    sudo setfacl -R -m default:user:2354:rwX,user:2354:rwX framework
    printf -- "-- Permission setted successfully. \n"

    # 2.4 Clone submodules
    printf -- "\n4. Clone submodules\n"
    printf -- "\n-- Changing directory to 'framework' .."
    cd framework
    printf -- "\n-- Initializing local configuration file ..\n"
    git submodule init
    printf -- "\n-- Fetching data from submodules ..\n"
    git submodule update

    
    # 2.5 Detect docker-compose
    printf -- "\n\n5. Detecting 'docker-compose'\n"
    printf -- "\n-- Installing 'docker-compose' .."
    pip3 install docker-compose
    printf -- "\n-- Successfully installed 'docker-compose'.\n"
    printf -- "\n-- 'docker-compose' detected!!" 
    printf -- "\n-- %s %s %s %s %s" $(docker-compose --version)

    # 2.6 Create Gitlab Access Tokens (to access registry) 
    printf -- "\n\n6. Create Gitlab access tokens\n"
    while true;
    do 
        printf -- "\n-- To generate Gitlab access token visit https://gitlab.com/profile/personal_access_tokens\n   and create a new access token with the 'read_repository' & 'read_registry' scope."
        printf -- "\n-- Make sure you have Developer access to CDLI Project. If not, then ask for developer access on #Slack\n"
        read -p "   Have you created Gitlab access token? [y|N] " yesNo

        if [[ $yesNo =~ ^(y|Y)$ ]]
        then
            printf -- "\n Save the Gitlab access token safely!!\n We will require the same token in Stage-3 Setup."
            break
        else
            printf -- "\nIt is mandatory to create access token, without which further installation will fail !!\n"
        fi
    done

    # 2.7 Login to Docker
    printf -- "\n\n7. Login to docker\n"        
    printf -- "\n-- Login using any one Link. After successfull login select Option '3) Quit' to continue with setup.\n\n"
    PS3="Option  : "
    options=("Link 1 : 'https://registry.gitlab.com/v2/cdli/framework/orchestrator/manifests/0.0.3'" "Link 2 : 'https://registry.gitlab.com/'" "Quit")
    select opt in "${options[@]}"
    do
        case $opt in
            "Link 1 :"*) 
                printf -- "\nExecuting docker login for Link 1\n" 
                docker login https://registry.gitlab.com/v2/cdli/framework/orchestrator/manifests/0.0.3
                ;;
            "Link 2 :"*) 
                printf -- "\nExecuting docker login for Link 2\n" 
                docker login https://registry.gitlab.com/
                ;;
            "Quit")
                printf -- "\n"
                break
                ;;
            *) printf -- "\ninvalid option $REPLY\n";;
        esac
    done

    printf -- "\n** To setup DB, first execute './framework/dev/cdlidev.py up'\n   and then run setup.sh for Stage-3 Setup.\n"        

}

# Stage-3 Setup
stage_3() {

    # 3.1 Detect Virtual Environment
    printf -- "\n1. Detecting active virtual environment\n"
    if [ "$VIRTUAL_ENV" == $CDLI_ENV_PATH ];
    then
        printf -- "\n-- Detected Virtual Environment : "$CDLI_ENV_NAME"\n"
    else
        activate_venv_msg 3
        stage_aborted 3
    fi 

    # 3.2 Detect cdlidev.py running status
    printf -- "\n2. Detecting cdlidev.py status\n"
    if [[ ! $(pgrep -f cdlidev.py) ]]; 
    then
        printf -- "\n-- 'cdlidev.py' status : not running !!\n"
        printf -- "\nERROR : Make sure framework/dev/cdlidev.py is runnuning and run setup.sh for Stage-3 Setup again!!\n"
        stage_aborted 3
    else
        printf -- "\n-- 'cdlidev.py' status : running  !!\n"
    fi

    # 3.3 Detect 'mariadb' container status 
    printf -- "\n3. Detecting 'mariadb' container status\n"
    if [ ! "$(docker ps -a | grep cdlidev_mariadb_1)" ]; 
    then
        printf -- "\n-- 'cdlidev_mariadb_1' container status : not running  !!\n"
        printf -- "\nERROR : Make sure 'cdlidev_mariadb_1' container is runnuning and run setup.sh for Stage-3 Setup again!!"
        stage_aborted 3
    else
        printf -- "\n-- 'cdlidev_mariadb_1' container status : up and running  !!\n"
    fi

    # 3.4 Detect 'phpmyadmin(pma)' container status 
    printf -- "\n4. Detecting 'phpmyadmin' container status\n"
    if [ ! "$(docker ps -a | grep cdlidev_pma_1)" ]; 
    then
        printf -- "\n-- 'cdlidev_pma_1' container status : not running  !!\n"
    else
        printf -- "\n-- 'cdlidev_pma_1' container status : up and running  !!\n"
    fi

    # 3.5 Extract Database
    printf -- "\n5. Extract Database\n"
    while true;
    do
        printf -- '\n a) Import from downloaded cdli_db.gz file.'
        printf -- '\n b) Import from Server (need Gitlab access token)\n'
        printf -- '\n** To generate Gitlab access token visit https://gitlab.com/profile/personal_access_tokens\n   and create a new access token with the "read_repository" & "read_registry" scope.\n\n'

        read -p "Do you have Gitlab access token or CDLI Gitlab Developer access? [y|N] " yesNo
        mkdir $SETUP_SH_DIR/.tmp
        docker_dbscript
        if [[ $yesNo =~ ^(y|Y)$ ]]
        then
            read -p "Enter Gitlab access token : " accessToken
            
            printf -- "\n-- Downloading cdli_db.gz ..\n"
            curl --header 'PRIVATE-TOKEN: '$accessToken 'https://gitlab.com/api/v4/projects/5973646/repository/files/cdli_db.gz/raw?ref=master' -o $SETUP_SH_DIR/.tmp/cdli_db.gz 

            if [ -f $SETUP_SH_DIR"/.tmp/cdli_db.gz" ]
            then
                fileSize=$(stat -c %s $SETUP_SH_DIR"/.tmp/cdli_db.gz")

                if [ "$fileSize" -gt "1000" ] 
                then
                    printf -- "\n-- Extracting cdli_db.gz ..\n"
                    gzip -vdc $SETUP_SH_DIR/.tmp/cdli_db.gz  > $SETUP_SH_DIR/.tmp/cdli_db.sql
                    printf -- "\n-- Successfully extracted cdli_db.gz to cdli_db.sql\n"
                    break
                else 
                    printf -- "\nERROR : Invalid access token. Try Again !!\n"
                    rm -r $SETUP_SH_DIR/.tmp
                fi
            else :
                printf -- "\nERROR : Downloading database failed. Try again !!\n"
                rm -r $SETUP_SH_DIR/.tmp
            fi
        else
            printf -- "\n-- Extract cdli_db.sql from cdli_db.gz\n"
            printf -- "\n-- **Copy the full path to cdli_db.gz and paste\n (Trick : Copy cdli_db.gz folder and Ctrl+Shift+v)\n"
            read -p "-- Path to cdli_db.gz : " DB_PATH

            if [ -f $DB_PATH ]
            then
                printf -- "\n-- Extracting cdli_db.gz ..\n"
                gzip -vdc $DB_PATH  > $SETUP_SH_DIR/.tmp/cdli_db.sql
                printf -- "\n-- Successfully extracted cdli_db.gz to cdli_db.sql\n"
                break
            else 
                printf -- "\nERROR : Invalid path. Try again !!\n"
                rm -r $SETUP_SH_DIR/.tmp
            fi
        fi
        rm -r $SETUP_SH_DIR/.tmp
    done
    
    # 3.6 Copy the required contents to 'mariadb' container 
    printf -- "\n6. Copy cdli_db.sql to 'cdlidev_mariadb_1' container\n"
    docker cp $SETUP_SH_DIR/.tmp/cdli_db.sql cdlidev_mariadb_1:/tmp/cdli_db.sql
    docker cp $SETUP_SH_DIR/.tmp/dbSetup.sh cdlidev_mariadb_1:/dbSetup.sh
    printf -- "\n-- Successfully copied to 'cdlidev_mariadb_1'\n"
    
    # 3.7 Opening 'mariadb' shell and execute commands
    printf -- "\n7. Open the MariaDB instance\n"
    printf -- "\n-- Starting 'cdlidev_mariadb_1' shell"
    docker exec -it cdlidev_mariadb_1 sh -c "chmod +x dbSetup.sh; ./dbSetup.sh; rm dbSetup.sh"

    # 3.8 Clean up cache
    printf -- "\n8. Cleaning up cache\n"
    if [ -d $SETUP_SH_DIR'/.tmp' ]
    then
        rm -r $SETUP_SH_DIR/.tmp
    fi

    printf -- "\nCDLI Framework Setup process completed.\nRe-run cdlidev.py\n"
}

# Packing start message, function call and finished message for each stage
stage_pack() {
    stage_start $1
    stage_$1
    stage_finished $1
    exit 0
}

# Display Header message
printf -- "\n*------------- CDLI Framework Setup -------------*\n"
printf -- "\n*-------------------------------------------------*"
printf -- "\n*---  Select option to get started with setup  ---*"
printf -- "\n*-------------------------------------------------*\n"

# Prompt user to select option
# 1. Stage 1 Setup
# 2. Stage 2 Setup (run inside virtualenv)
# 3. Stage 3 Setup (run inside virtualenv)
PS3="Option : "
options=("Stage 1 Setup" "Stage 2 Setup (run inside virtualenv)" "Stage 3 Setup DB (run inside virtualenv)" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Stage 1"*)
            stage_pack 1
            ;;
        "Stage 2"*)
            stage_pack 2
            ;;
        "Stage 3"*)
            stage_pack 3
            ;;
        "Quit")
            printf -- "\nExiting Framework Setup !!\n"
            break
            ;;
        *) printf -- "\ninvalid option $REPLY\n";;
    esac
done