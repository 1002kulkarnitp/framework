# Testing CDLI Framework

The Testing process of the framework currently includes the Build+Installation tests and the PHP, CSS and SCSS Lints tests. These tests ensure that the installation+build process is in working state and helps us in keeping up with the latest coding standards.

CDLI tests run at the GitLab CI/CD pipeline, which you can also run locally or at your git branch, [https://docs.gitlab.com/runner/](https://docs.gitlab.com/runner/). For the lint tests, necessary installations are done at dev_cake_composer container.

## Running Lint Tests

The below commands run the combined tests for linting PHP, CSS and SCSS files at the docker container, the results of the tests will be displayed at the same output screen, if failed please go through the error details for a fix, please use the below command at the root directory.

```python
python tests/docker-run-lint-tests.py -c
```

Flag `-c` will run the combined tests, you can use `--csslint, --scsslint & --phplint` flags to run individual tests, also see the help command `-h` to know more.
for the `linux` users, use the below quick command at the root directory to run all tests.

```bash
./cdlitest
```

If you make a commit to the CDLI Branch or PR without running the lint tests locally the pipeline may fail at the PR hence, before checking in any commits to the main repository, please ensure that every single test passes by following the above steps.

## Troubleshooting Tests

1. `Error response from daemon`, if you get this error please make sure the docker is up & running. Please connect to us via slack, incase you run through any issues.

2. `Python3 not found`, if you get this error pull the latest pheonix/develop branch, remove and rebuild the containers.

```bash
./dev/cdlidev.py up -- --build
```

## Contributing to CDLI Tests

We always appreciate help with writing more tests, if you want to contribute on the same please let us know. If you have written any feature make sure to update the tests as appropriate.
